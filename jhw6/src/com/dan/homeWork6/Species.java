package com.dan.homeWork6;

public enum Species {
    DOG,
    ROBOCAT,
    DOMESTICCAT,
    BIRD,
    FISH,
    UNKNOWN
}

