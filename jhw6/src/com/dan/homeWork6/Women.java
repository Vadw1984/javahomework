package com.dan.homeWork6;

final class Women extends Human {
    public Pet pet;

    Women(String name, String surname, int year) {
        super(name, surname, year);
    }

    Women(String name, String surname, int year, short iq, String schedule[][]) {
        super(name, surname, year, iq, schedule);
    }

    Women(String name, String surname, int year, short iq, String schedule[][], Pet pet) {
        super(name, surname, year, iq, schedule);
        this.pet = pet;
    }
    public void makeup(){
        System.out.println("makeup");
    }

    @Override
    public void greetPet() {
        System.out.println("Привет ");
    }
}
