package com.dan.homeWork6;

public class RoboCat extends Pet implements Foul{
    public Species species = Species.ROBOCAT;     // вид животного
    private String nickname;

    RoboCat(String nickname, int age, short trickLevel, String[] habits){
        super(nickname,age,trickLevel,habits);
        this.nickname=nickname;
    }

    void respond(){
        System.out.printf("Привет, хозяин. Я %s. Меня зовут %s!\n",species.name(),nickname);
    }
    @Override
    public void doFoul(){
        System.out.println("Нужно хорошо замести следы...\n");
    }
}
