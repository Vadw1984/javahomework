package com.dan.homeWork6;

import java.util.Arrays;


public abstract class Pet{
    private int age;            // возраст
    private String nickname;    // кличка
    private short trickLevel;   // уровень хитрости
    private String[] habits;    // привычки
    private String isTick;

    Pet(){

    }

    Pet(String nickname, int age, short trickLevel, String[] habits){
        this.nickname = nickname;
        this.age=age;
        this.trickLevel=trickLevel;
        this.habits=habits;
    }

    public void setTrickLevel(short trickLevel) {
        this.trickLevel = trickLevel;
    }
    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public String isTrick(){
        if(trickLevel>50){
            return isTick="очень хитрый";
        } else return isTick="почти не хитрый";
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public String getIsTick() {
        isTrick();
        return isTick;
    }

    void eat(){
        System.out.printf("Я кушаю!\n");
    }

    abstract void respond();



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pet)) return false;

        Pet pet = (Pet) o;

        if (age != pet.age) return false;
        return nickname.equals(pet.nickname);
    }

    @Override
    public int hashCode() {
        int result = age;
        result = 31 * result + nickname.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Pet {" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
//                ", species='" + species + '\'' +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }
    @Override
    protected void finalize ( ) {
        System.out.println(this);
    }
}
