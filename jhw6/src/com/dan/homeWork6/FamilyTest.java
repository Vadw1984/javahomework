package com.dan.homeWork6;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FamilyTest {
    Man Petya = new Man("Petya","Zrorodny",1986);
    Women Natasha = new Women("Natasha","Zrorodny",1986);
    Women Iren = new Women("Iren","Iren",1986);
    Family module = new Family(Petya,Natasha);
    Family analogModule = new Family(Petya,Natasha);


    @Before
    public void method(){
        module.addChild(Iren);
    }

     @Test
    public void testCheckTheChildIsDeleteSuccessByObject(){
        module.deleteChild(Iren);
        module.deleteChild(Petya);

        assertArrayEquals(analogModule.getChildren(),module.getChildren());
    }

    @Test
    public void testCheckTheChildIsDeleteSuccessById(){
        module.addChild(Iren);

        module.deleteChild(0);
        module.deleteChild(6);

        assertEquals(3,module.countFamily());
    }

    @Test
    public void testArrayChildrenIncrementOnCurrentElement(){
        assertEquals(module.getChildren()[0],Iren);
    }

    @Test
    public void testCurrentFamilyMembers(){
        assertEquals(3,module.countFamily());
    }
}
