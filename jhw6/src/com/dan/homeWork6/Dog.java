package com.dan.homeWork6;

class Dog extends Pet implements Foul{
    public Species species = Species.DOG;     // вид животного
    private String nickname;

    Dog(String nickname, int age, short trickLevel, String[] habits){
        super(nickname,age,trickLevel,habits);
        this.nickname=nickname;
    }

    void respond(){
        System.out.printf("Привет, хозяин. Я %s. Меня зовут %s, Я машу хвостом!\n",species.name(),nickname);
    }
    @Override
    public void doFoul(){
        System.out.println("Нужно хорошо замести следы...\n");
    }

}
