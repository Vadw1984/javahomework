package com.dan.homeWork6;

public class Fish extends Pet {
    public Species species = Species.FISH;     // вид животного
    private String nickname;

    Fish(String nickname, int age, short trickLevel, String[] habits){
        super(nickname,age,trickLevel,habits);
        this.nickname=nickname;
    }

    void respond(){
        System.out.printf("Привет, хозяин. Я %s. Меня зовут %s, Я плаваю!\n",species.name(),nickname);
    }

}
