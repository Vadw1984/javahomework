package com.dan.homeWork6;

final class Man extends Human{
    public Pet pet;

    Man(String name, String surname, int year){
        super(name,surname,year);
    }

    Man(String name, String surname,int year,short iq,String schedule[][]){
        super(name,surname,year,iq,schedule);
    }

    Man(String name, String surname,int year,short iq,String schedule[][],Pet pet){
        super(name,surname,year,iq,schedule);
        this.pet=pet;
    }

    public void repairCar(){
        System.out.println("repairCar ");
    }

    @Override
    public void greetPet(){
        System.out.printf("Привет, %s \n",pet.getNickname());
    }

}
