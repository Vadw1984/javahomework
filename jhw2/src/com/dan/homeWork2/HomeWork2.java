package com.dan.homeWork1;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HomeWork2 {
    static Scanner sc = new Scanner(System.in);
    static int[] shoot = new int[2];
    static int [][]field = new int[6][6];
    static int[] target = {2,4};


    public static void main(String[] args) {
        System.out.println("All set. Get ready to rumble!\n");
        start();
    }

    public static void start() {
        displayStringField();
        makeShoot();

    }

    public static Object createField() {
        for (int i = 0; i < field.length; i++) {
            field[0][i]=+i;
            field[i][0]=+i;
        }
        return field;
    }

    public static void displayStringField() {
        createField();

        for (int[] ints : field) {
            StringBuffer stringField = new StringBuffer(Arrays.toString(ints).replace("0","-").replace("-1","*").replace("-3","X"));
            Pattern pattern = Pattern.compile("[],]");
            Matcher matcher = pattern.matcher(stringField.replace(0,1,""));
            String value = matcher.replaceAll(" |");
            System.out.println(value);
        }
    }

    public static void scanShoot(String shootDirection) {
        System.out.printf("Choose your %s for shoot! It must be number between 1 to %d \n",shootDirection,field.length-1);

      if(sc.nextInt()<0&&sc.hasNextInt()&&shootDirection=="Line"){
        shoot[0] = sc.nextInt();
        } else if(sc.hasNextInt()&&shootDirection=="Column") {
            shoot[1] = sc.nextInt();
        }else        {
            System.out.printf("It must be NUMBER! between 1 to %d \n",field.length-1);
        }
    }

    public static void makeShoot() {
        scanShoot("Line");
        scanShoot("Column");

        if(Arrays.toString(shoot).equals(Arrays.toString(target))){
            field[shoot[0]][shoot[1]]=-3;
            displayStringField();
            System.out.printf("You have won!");

        }else {
            field[shoot[0]][shoot[1]]=-1;
            start();
        }


    }
}
