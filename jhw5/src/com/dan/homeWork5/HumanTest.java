package com.dan.homeWork5;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HumanTest {
    @Test
    public void testCorrectWorkToString(){
        Human Sanek = new Human("Oleksandr","Pushkin",25);

        assertEquals("Human{name='Oleksandr', surname='Pushkin', year=25, iq=130, schedule=null}",Sanek.toString());
    }
}
