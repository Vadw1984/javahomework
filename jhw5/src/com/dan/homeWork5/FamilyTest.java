package com.dan.homeWork5;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FamilyTest {
    Human Petya = new Human("Petya","Zrorodny",1986);
    Human Natasha = new Human("Natasha","Zrorodny",1986);
    Human Iren = new Human("Iren","Iren",1986);
    Family module = new Family(Petya,Natasha);
    Family analogModule = new Family(Petya,Natasha);


    @Before
    public void method(){
        module.addChild(Iren);
    }

     @Test
    public void testCheckTheChildIsDeleteSuccessByObject(){
        module.deleteChild(Iren);
        module.deleteChild(Petya);

        assertArrayEquals(analogModule.getChildren(),module.getChildren());
    }

    @Test
    public void testCheckTheChildIsDeleteSuccessById(){
        module.addChild(Iren);

        module.deleteChild(0);
        module.deleteChild(6);

        assertEquals(3,module.countFamily());
    }

    @Test
    public void testArrayChildrenIncrementOnCurrentElement(){
        assertEquals(module.getChildren()[0],Iren);
    }

    @Test
    public void testCurrentFamilyMembers(){
        assertEquals(3,module.countFamily());
    }
}
