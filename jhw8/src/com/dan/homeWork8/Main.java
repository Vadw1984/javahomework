package com.dan.homeWork8;

import com.dan.homeWork8.controller.FamilyController;
import com.dan.homeWork8.dao.CollectionFamilyDao;
import com.dan.homeWork8.dao.FamilyDao;
import com.dan.homeWork8.entities.family.Family;
import com.dan.homeWork8.entities.human.DayOfWeek;
import com.dan.homeWork8.entities.human.Man;
import com.dan.homeWork8.entities.human.Women;
import com.dan.homeWork8.entities.pet.Dog;
import com.dan.homeWork8.entities.pet.DomesticCat;
import com.dan.homeWork8.entities.pet.Fish;
import com.dan.homeWork8.entities.pet.RoboCat;
import com.dan.homeWork8.service.FamilyService;

import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        int countStr = 0;

        String[][] scheduleMichael = {
                {DayOfWeek.SUNDAY.name(), "task"},
                {DayOfWeek.MONDAY.name(), "task_2"},
                {DayOfWeek.TUESDAY.name(), "task3"},
                {DayOfWeek.WEDNESDAY.name(), "task4"},
                {DayOfWeek.THURSDAY.name(), "task5"},
                {DayOfWeek.FRIDAY.name(), "task6"},
                {DayOfWeek.SATURDAY.name(), "task7"},
        };

        Man Vadim = new Man("Vadim", "Vadva", 33);
        Man Petya = new Man("Petya", "Zrorodny", 5);
        Women Jane_Karleone = new Women("Jane", "Karleone", 67);
        Man Vito_Karleone = new Man("Vito", "Karleone", 65);
        Man Michael = new Man("Michael", "Karleone", 45, (short) 60, new HashMap<String, String>(Arrays.stream(scheduleMichael).collect(Collectors.toMap(e -> e[0], e -> e[1]))));
        Women Iren = new Women("Iren", "Bevziuk", 33);
        Women Julia = new Women("Julia", "Taran", 54);

        Dog Mustang = new Dog("Mustang ", 7, (short) 60, new HashSet<String>(Arrays.asList("eat", "sleep", "drink")));
        DomesticCat Tom = new DomesticCat("Tom", 3, (short) 40, new HashSet<String>(Arrays.asList("eat", "sleep", "drink")));
        Fish Gold = new Fish("Gold", 5, (short) 70, new HashSet<String>(Arrays.asList("eat", "sleep")));
        Gold.respond(Gold.getNickname());
        RoboCat Jerry = new RoboCat("Jerry", 7, (short) 90, new HashSet<String>(List.of("sleep", "drink")));
        Jerry.respond(Jerry.getNickname());

        Family Mafia = new Family(Vito_Karleone, Jane_Karleone, Mustang);
        Family Ivanov = new Family(Michael, Iren);

//        Man Dima = new Man("Dima","Karleonito",1981, (short) 60,new HashMap<String, String>(Arrays.stream(scheduleMichael).collect(Collectors.toMap(e -> e[0], e -> e[1]))),Jerry);
//        Women Irina = new Women("Irina","Karleonito",1984, (short) 90,new HashMap<String, String>(Arrays.stream(scheduleMichael).collect(Collectors.toMap(e -> e[0], e -> e[1]))),Gold);
//        Dima.setPet(Gold);
//        Dima.greetPet(Dima.getPet());
//        Irina.setPet(Jerry);
//        Irina.greetPet(Irina.getPet());
//        System.out.println(Dima.getSchedule());
//

//        Mafia.addChild(Iren);
//        Mafia.addChild(Vadim);
//        Mafia.addChild(Petya);
//        Mafia.addChild(Michael);
//        Mafia.deleteChild(1);
//        Mafia.getChildren().get(0).displayInfo();
//
//        System.out.println("=============================3");
//        Mafia.addPet(Jerry);
//        Mafia.deletePet(Jerry);
//        System.out.println(Mafia);
//
//        System.out.println("=============================4");
//        show(Vito_Karleone, Jane_Karleone, Mustang);

//        System.out.println("=============================4");
        FamilyDao dao = new CollectionFamilyDao();
        FamilyService service = new FamilyService(dao);
        FamilyController controller = new FamilyController(service);
        System.out.printf("================================================================%d\n", ++countStr);
        controller.saveFamily(Mafia);
        controller.saveFamily(Ivanov);
        controller.getAllFamilies().forEach(System.out::println);
        controller.displayAllFamilies();
        System.out.printf("================================================================%d\n", ++countStr);
        Mafia.addChild(Petya);
        controller.getFamiliesBiggerThan(3);
        controller.getFamiliesLessThan(3);
        controller.countFamiliesWithMemberNumber(3);
        System.out.printf("================================================================%d\n", ++countStr);
        controller.createNewFamily(Vadim, Julia);
        controller.displayAllFamilies();
        System.out.printf("================================================================%d\n", ++countStr);
        controller.deleteFamilyByIndex(1);
        controller.displayAllFamilies();
        System.out.printf("================================================================%d\n", ++countStr);
        System.out.println(controller.bornChild(Mafia, "Cris", "Jully"));
        controller.displayAllFamilies();
        System.out.printf("================================================================%d\n", ++countStr);
        System.out.println(controller.adoptChild(Mafia, Petya));
        System.out.printf("================================================================%d\n", ++countStr);
        controller.deleteAllChildrenOlderThen(controller.getAllFamilies(), 3);
        controller.displayAllFamilies();
        System.out.printf("================================================================%d\n", ++countStr);
        System.out.println(controller.count(controller.getAllFamilies()));
        System.out.printf("================================================================%d\n", ++countStr);
        System.out.println(controller.getFamilyById(0));
        System.out.printf("================================================================%d\n", ++countStr);
        System.out.println(controller.getPets(controller.getAllFamilies(), 0));
        System.out.printf("================================================================%d\n", ++countStr);
        controller.addPet(controller.getAllFamilies(), 0, Gold);
        controller.displayAllFamilies();

    }


//    public static void show(Human human1,Human human3,Pet pet1) {
//        for (int i = 0; i < 10000; i++) {
//            Family Mafia=new Family(human1,human3,pet1);
//        }
//    };
}





