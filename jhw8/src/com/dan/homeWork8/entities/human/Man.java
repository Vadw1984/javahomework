package com.dan.homeWork8.entities.human;

import com.dan.homeWork8.entities.pet.Pet;

import java.util.Map;

public final class Man extends Human {

    public Man(String name, String surname, int year){
        super(name,surname,year);
    }

    public Man(String name, String surname,int year,short iq,Map<String,String> schedule){
        super(name,surname,year,iq,schedule);
    }

    public  Man(String name, String surname, int year, short iq, Map<String,String> schedule, Pet pet){
        super(name,surname,year,iq,schedule);
    }

    public void repairCar(){
        System.out.println("repairCar ");
    }

    @Override
    public void greetPet(Pet pet){
        System.out.printf("Привет, %s \n",pet.getNickname());
    }

}
