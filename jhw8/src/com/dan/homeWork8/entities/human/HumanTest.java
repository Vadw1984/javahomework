package com.dan.homeWork8.entities.human;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HumanTest {
    @Test
    public void testCorrectWorkToString(){
        Man Sanek = new Man("Oleksandr","Pushkin",25);

        assertEquals("Human{name='Oleksandr', surname='Pushkin', year=25, iq=130, schedule={}}",Sanek.toString());
    }
}
