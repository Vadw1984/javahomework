package com.dan.homeWork8.entities.human;

import com.dan.homeWork8.entities.pet.Pet;

import java.util.Map;

public final class Women extends Human {

    public Women(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Women(String name, String surname, int year, short iq, Map<String,String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Women(String name, String surname, int year, short iq, Map<String,String> schedule, Pet pet) {
        super(name, surname, year, iq, schedule);
    }
    public void makeup(){
        System.out.println("makeup");
    }

    @Override
    public void greetPet() {
        System.out.println("Привет ");
    }
}
