package com.dan.homeWork8.entities.pet;

import com.dan.homeWork8.entities.human.Foul;

import java.util.Set;

public class RoboCat extends Pet implements Foul {
    private Species species = Species.ROBOCAT;     // вид животного

    public RoboCat(String nickname, int age, short trickLevel, Set<String> habit){
        super(nickname,age,trickLevel,habit);
    }

    public void respond(String nickname){
        System.out.printf("Привет, хозяин. Я %s. Меня зовут %s, Я машу хвостом!\n",species.name(),nickname);
    }

    @Override
    public void doFoul(){
        System.out.println("Нужно хорошо замести следы...\n");
    }
}
