package com.dan.homeWork8.entities.pet;

import com.dan.homeWork8.entities.human.Foul;

import java.util.Set;

public class DomesticCat extends Pet implements Foul {


    public  DomesticCat(
            String nickname, int age, short trickLevel, Set<String> habit){
        super(nickname,age,trickLevel,habit);
        super.setSpecies(Species.DOMESTICCAT);
    }

    void respond(String nickname){
        System.out.printf("Привет, хозяин. Я %s. Меня зовут %s, Я машу хвостом!\n",getSpecies().name(),nickname);
    }
    @Override
    public void doFoul(){
        System.out.println("Нужно хорошо замести следы...\n");
    }
}
