package com.dan.homeWork8.entities.pet;

public enum Species {
    DOG,
    ROBOCAT,
    DOMESTICCAT,
    BIRD,
    FISH,
    UNKNOWN
}

