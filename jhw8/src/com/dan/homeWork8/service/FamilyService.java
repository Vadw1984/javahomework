package com.dan.homeWork8.service;

import com.dan.homeWork8.entities.family.Family;
import com.dan.homeWork8.dao.FamilyDao;
import com.dan.homeWork8.entities.human.Human;
import com.dan.homeWork8.entities.human.Man;
import com.dan.homeWork8.entities.human.Women;
import com.dan.homeWork8.entities.pet.Pet;

import java.util.*;

public class FamilyService {

    public FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void saveFamily(Family family) {
        familyDao.saveFamily(family);
    }

    public void displayAllFamilies() {
        for (Family family : familyDao.getAllFamilies()) {
            System.out.println(family);
        }
    }

    public void getFamiliesBiggerThan(int num) {
        List<Family> familyListWithMemberNumber = new ArrayList<>();

        for (Family family : familyDao.getAllFamilies()) {
            if (family.countFamily() > num) {
                familyListWithMemberNumber.add(family);
            }
        }

        if (familyListWithMemberNumber.size() > 0) {
            System.out.println(familyListWithMemberNumber);

        } else {
            System.out.printf("there are no families bigger than %d humans\n", num);
        }
    }

    public void getFamiliesLessThan(int num) {
        List<Family> familyListWithMemberNumber = new ArrayList<>();

        for (Family family : familyDao.getAllFamilies()) {
            if (family.countFamily() < num) {
                familyListWithMemberNumber.add(family);
            }
        }

        if (familyListWithMemberNumber.size() > 0) {
            System.out.println(familyListWithMemberNumber);
        } else {
            System.out.printf("there are no families with humans less than %d\n", num);
        }
    }

    public void countFamiliesWithMemberNumber(int num) {
        List<Family> familyListWithMemberNumber = new ArrayList<>();

        for (Family family : familyDao.getAllFamilies()) {
            if (family.countFamily() == num) {
                familyListWithMemberNumber.add(family);
            }
        }

        if (familyListWithMemberNumber.size() > 0) {
            System.out.println(familyListWithMemberNumber);
        } else {
            System.out.printf("there are no families with %d humans \n", num);
        }
    }

    public void createNewFamily(Human human1, Human human2) {
        Family familyNew = new Family(human1, human2);
        familyDao.saveFamily(familyNew);
    }

    public void deleteFamilyByIndex(int index) {
        familyDao.deleteFamilyByIndex(index);
    }

    public void deleteFamilyByObject(Family family) {
        familyDao.deleteFamily(family);
    }

    public Family bornChild(Family family, String manName, String womenName) {
        Random random = new Random();
        int randomNum = random.nextInt(2);

        if (randomNum == 1) {
            Human newMan = new Man(manName, family.getFather().getSurname(), 0);
            if (familyDao.getAllFamilies().contains(family)) {
                int ind = getAllFamilies().indexOf(family);
                family.addChild(newMan);
                familyDao.getAllFamilies().set(ind, family);
                return family;
            } else {
                family.addChild(newMan);
                saveFamily(family);
                return family;
            }
        } else {
            Human newWoman = new Women(womenName, family.getFather().getSurname(), 0);
            if (getAllFamilies().contains(family)) {
                int ind = familyDao.getAllFamilies().indexOf(family);
                family.addChild(newWoman);
                familyDao.getAllFamilies().set(ind, family);
                return family;
            } else {
                family.addChild(newWoman);
                saveFamily(family);
                return family;
            }
        }
    }

    public Family adoptChild(Family family, Human human) {
        human.setSurname(family.getFather().getSurname());
        if (familyDao.getAllFamilies().contains(family)) {
            int ind = familyDao.getAllFamilies().indexOf(family);
            family.addChild(human);
            familyDao.getAllFamilies().set(ind, family);
            return family;
        } else {
            family.addChild(human);
            familyDao.saveFamily(family);
            return family;
        }
    }

    public void deleteAllChildrenOlderThen(List<Family> familyList, int age) {

        Iterator<Family> familyIterator = familyList.listIterator();

        while (familyIterator.hasNext()) {
            Iterator<Human> childIterator = familyIterator.next().getChildren().listIterator();

            while (childIterator.hasNext()) {
                if (childIterator.next().getYear() > age) {
                    childIterator.remove();
                }
            }
        }
//        List <Human> humanChildrenOlderThen = new ArrayList<>();
//        familyList.forEach(e ->
//                e.getChildren().forEach((el) -> {
//                    if (el.getYear() > age) {
//                                humanChildrenOlderThen.add(el);
//                                System.out.println(humanChildrenOlderThen);
//                            }
//                        }
//                )
//        );

//        familyList.forEach(e ->
//                e.getChildren().removeAll(humanChildrenOlderThen)
//        );
    }

    public int count(List<Family> familyList) {
        return familyList.size();
    }

    public Family getFamilyById(int index) {
        return familyDao.getFamilyById(index);
    }

    public LinkedHashSet<Pet> getPets(List<Family> familyList, int index) {
        return familyList.get(index).getPets();
    }

    public void addPet(List<Family> familyList, int index, Pet pet) {
        familyList.get(index).getPets().add(pet);
    }
}
