package com.dan.homeWork8.dao;

import java.util.List;

import com.dan.homeWork8.entities.family.Family;

public interface FamilyDao {
    List<Family> getAllFamilies();

    Family getFamilyById(int index);

    void saveFamily(Family family);

    void deleteFamilyByIndex(int index);

    boolean deleteFamily(Family family);
}
