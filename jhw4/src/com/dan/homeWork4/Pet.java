package com.dan.homeWork4;

import java.util.Arrays;


class Pet{
    private int age;            // возраст
    final String species;     // вид животного
    private String nickname;    // кличка
    private short trickLevel;   // уровень хитрости
    private String[] habits;    // привычки
    private String isTick;


    Pet(String nickname,String species){
        this.nickname = nickname;
        this.age=age;
        this.species = species;;
    }

    Pet(String nickname, int age,String species,short trickLevel,String[] habits){
        this.nickname = nickname;
        this.age=age;
        this.species=species;
        this.trickLevel=trickLevel;
        this.habits=habits;
    }

    public void setTrickLevel(short trickLevel) {
        this.trickLevel = trickLevel;
    }
    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public String isTrick(){
        if(trickLevel>50){
            return isTick="очень хитрый";
        } else return isTick="почти не хитрый";
    }

    public String getNickname() {
        return nickname;
    }
    public String getSpecies() {
        return species;
    }

    public int getAge() {
        return age;
    }

    public String getIsTick() {
        isTrick();
        return isTick;
    }

    void displayInfoAboutEating(){
        System.out.printf("Я кушаю!\n");
    }
    void displayRespondOnOwner(){
        System.out.printf("Привет, хозяин. Я %s. Я соскучился!\n", nickname);
    }
    void doFoul(){
        System.out.println("Нужно хорошо замести следы...\n");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pet)) return false;

        Pet pet = (Pet) o;

        if (age != pet.age) return false;
        if (!species.equals(pet.species)) return false;
        return nickname.equals(pet.nickname);
    }

    @Override
    public int hashCode() {
        int result = age;
        result = 31 * result + species.hashCode();
        result = 31 * result + nickname.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return ""+species+" {" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", species='" + species + '\'' +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
        }
    }