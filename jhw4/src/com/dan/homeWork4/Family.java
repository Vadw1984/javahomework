package com.dan.homeWork4;

import java.util.Arrays;

class Family extends Human{

    final Human mother;                                               // mother
    final Human father;                                               // father
    private Pet pet;                                                  // pet
    private Human[] children={};
    static int counterFamily=2;

    Family( Human father,Human mother){
        this.mother=mother;
        this.father=father;
    };

    Family(Human father,Human mother,Pet pet){

        this.mother=mother;
        this.father=father;
        this.pet=pet;
    };

    public Human getMother() {
        return mother;
    }
    public Human getFather() {
        return father;
    }
    public Human[] getChildren() {
        return children;
    }
    public Pet getPet() {
        return pet;
    }
    public void setPet(Pet pet){
        this.pet = pet;
    }

    void greetPet(){
        System.out.printf("\"Привет, %s \n",pet.getNickname());
    }
    void describePet(){
    System.out.printf("У меня есть %s, ему %d лет, он %s\n", pet.getSpecies(),pet.getAge(),pet.getIsTick());
}
    void addChild(Human newChild){
        Human[] newChildrenArr = Arrays.copyOf(children,children.length+1);
        newChildrenArr[children.length] = newChild;
        children=newChildrenArr;
    }

        public boolean deleteChild(int index){
            if(children.length<=index){
                System.out.println("there is no child");
                return false;
            } else {
                Human[] ChildArrayBeforeRemoveChild = Arrays.copyOf(children,children.length-1);
                System.arraycopy(children,index+1,ChildArrayBeforeRemoveChild,index,children.length-1-index);
                System.arraycopy(children,0,ChildArrayBeforeRemoveChild,0,children.length-(children.length-index));
                children=ChildArrayBeforeRemoveChild;
                return true;
        }
    }
    public int countFamily() {
        counterFamily=2;
        if (pet != null) {
            return counterFamily += children.length+1;
        } else return counterFamily +=children.length;
    };

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother.toString() +
                ", father=" + father.toString()  +
                ", children=" + Arrays.toString(children) +
                ", pet=" + pet +
                '}';
        }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;
        if (!super.equals(o)) return false;

        Family family = (Family) o;

        if (mother != null ? !mother.equals(family.mother) : family.mother != null) return false;
        return father != null ? father.equals(family.father) : family.father == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (mother != null ? mother.hashCode() : 0);
        result = 31 * result + (father != null ? father.hashCode() : 0);
        return result;
    }
}