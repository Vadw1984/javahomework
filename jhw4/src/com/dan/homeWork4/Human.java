package com.dan.homeWork4;

import java.util.Arrays;

class Human{
    private String name;                                                // имя
    private String surname;                                             // фамилия
    private int year;                                                   // возраст
    private short iq;                                                   // iq
    private String schedule[][];                                        // schedule
    private Family family;

    Human(){

    }

    Human(String name, String surname,int year){

        this.name = name;
        this.surname = surname;
        this.year=year;
        this.iq = 130;

    }

    Human(String name, String surname,int year,short iq,String schedule[][]){
        this.name = name;
        this.surname = surname;
        this.year=year;
        this.iq=iq;
        this.schedule=schedule;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    public short getIq() {
        return iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setIq(short iq) {
        this.iq = iq;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }


    public void  displayInfo(){
        System.out.printf("Name: %s \tAge: %d\n", this.name, this.year);
    }

    public int  displayFamilyCount(){
       return Family.counterFamily;
    }


    @Override
    public String toString() {

        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.deepToString(schedule)+
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;

        Human human = (Human) o;

        if (year != human.year) return false;
        if (iq != human.iq) return false;
        if (!name.equals(human.name)) return false;
        if (!surname.equals(human.surname)) return false;
        return Arrays.deepEquals(schedule, human.schedule);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + surname.hashCode();
        result = 31 * result + year;
        result = 31 * result + (int) iq;
        result = 31 * result + Arrays.deepHashCode(schedule);
        return result;
    }
}