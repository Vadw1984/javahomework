package com.dan.homeWork4;

import java.util.Arrays;

class Main {

    public static void main(String[] args) {
        Human Vadim = new Human("Vadim","Vadva",1983);
        Vadim.displayInfo();
        Human Petya = new Human("Petya","Zrorodny",1986);
        Petya.displayInfo();
        Human Jane_Karleone=new Human("Jane","Karleone",1897 );
        Jane_Karleone.displayInfo();
        Human Vito_Karleone=new Human("Vito","Karleone",1891 );
        Vito_Karleone.displayInfo();

        String[][] scheduleMichael ={{"day", "task"},{"day_2", "task_2"}};
        Human Michael = new Human("Michael","Karleone",1977, (short) 60,scheduleMichael);
        Michael.displayInfo();
        Human Iren = new Human("Iren","Bevziuk",1986);

        Pet Mustang = new Pet("Mustang ",7,"Toy terrier", (short) 60, new String[]{"eat", "sleep", "drink"});

        Family Mafia=new Family(Vito_Karleone,Jane_Karleone,Mustang);
        System.out.println(Mafia.toString());

        Mafia.addChild(Iren);
        Mafia.addChild(Vadim);
        Mafia.addChild(Petya);
        Mafia.addChild(Michael);

        System.out.println(Mafia.toString());
        System.out.println(Mafia.countFamily());
        Mafia.deleteChild(1);
        System.out.println(Mafia.toString());
        System.out.println(Mafia.countFamily());

        System.out.println(Mafia.getChildren()[0].getName());
        System.out.println(Mafia.getChildren()[0].toString());
        System.out.println(Mafia.getChildren()[0].toString());
        Mafia.getChildren()[0].displayInfo();
        System.out.println(Mafia.getChildren()[0].displayFamilyCount());
    }
}





