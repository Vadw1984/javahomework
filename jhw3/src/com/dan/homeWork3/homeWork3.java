package com.dan.homeWork1;

import java.util.Scanner;

public class homeWork3 {

    static String[][] schedule = new String[7][2];
    static Scanner scanner = new Scanner(System.in);
    static String answer=new String();

    public static void main(String[] args) {
        createSchedule();
        lookIntoYourSchedule();
    }

    private static void lookIntoYourSchedule() {
        askForDay();
        if("exit".compareToIgnoreCase(answer)==0){
            scanner.close();
        }else checkTheAnswer();
    }

    private static void askForDay() {
        System.out.println("Please, input the day of the week:\n");
        answer=scanner.nextLine().replace(" ","");

        for (String[] strings : schedule) {
            if(answer.regionMatches(true, 0, strings[0], 0, 3)){
                answer=strings[0];
            };
        }
    }

    private static void createSchedule() {
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = " attend school in the morning and return home in the afternoon ";
        schedule[3][0] = "Wednesday ";
        schedule[3][1] = "go to courses or work and return home to watch television, play video games, make plans with friends, spend time with family, read, or engage in a similar leisure-related activity";
        schedule[4][0] = "Thursday ";
        schedule[4][1] = "return to work ";
        schedule[5][0] = "Friday ";
        schedule[5][1] = " close to the end all tasks of the work week";
        schedule[6][0] = "Saturday ";
        schedule[6][1] = "is free to stay out until late at night, having fun with plans or other leisure-related activities";
    }

    private static void checkTheAnswer() {
            switch (answer){
                case "Sunday" ->  System.out.println(schedule[0][1]+"\n");
                case "Monday" -> System.out.println(schedule[1][1]+"\n");
                case "Tuesday" ->System.out.println(schedule[2][1]+"\n");
                case "Wednesday" ->System.out.println(schedule[3][1]+"\n");
                case "Thursday" ->System.out.println(schedule[4][1]+"\n");
                case "Friday" ->  System.out.println(schedule[5][1]+"\n");
                case "Saturday" -> System.out.println(schedule[6][1]+"\n");
                default -> System.out.println("Sorry, I don't understand you, please try again.");
        }
        lookIntoYourSchedule();
    }
}
