package dan.homeWork7;

import java.util.*;
import java.util.stream.Collectors;

class Main {

    public static void main(String[] args) {
        Man Vadim = new Man("Vadim","Vadva",1983);
        Man Petya = new Man("Petya","Zrorodny",1986);
        Women Jane_Karleone=new Women("Jane","Karleone",1897 );
        Man Vito_Karleone=new Man("Vito","Karleone",1891 );

        String[][] scheduleMichael ={
                {DayOfWeek.SUNDAY.name(), "task"},
                {DayOfWeek.MONDAY.name(), "task_2"},
                {DayOfWeek.TUESDAY.name(), "task3"},
                {DayOfWeek.WEDNESDAY.name(), "task4"},
                {DayOfWeek.THURSDAY.name(), "task5"},
                {DayOfWeek.FRIDAY.name(), "task6"},
                {DayOfWeek.SATURDAY.name(), "task7"},
        };

        Man Michael = new Man("Michael","Karleone",1977, (short) 60,new HashMap<String, String>(Arrays.stream(scheduleMichael).collect(Collectors.toMap(e -> e[0], e -> e[1]))));
        Women Iren = new Women("Iren","Bevziuk",1986);
        Dog Mustang = new Dog("Mustang ",7, (short) 60, new HashSet<String>(Arrays.asList("eat", "sleep", "drink")));
        Mustang.respond(Mustang.getNickname());
        DomesticCat Tom = new DomesticCat("Tom",3, (short) 40, new HashSet<String>(Arrays.asList("eat", "sleep", "drink")));
        System.out.println(Tom.getHabits().size());
        System.out.println(Tom);

        Fish Gold = new Fish("Gold",5, (short) 70,new HashSet<String>(Arrays.asList("eat", "sleep")));
        Gold.respond(Gold.getNickname());
        RoboCat Jerry = new RoboCat("Jerry",7, (short) 90,new HashSet<String>(List.of("sleep", "drink")));
        Jerry.respond(Jerry.getNickname());

        System.out.println("=============================1");
        Family Mafia=new Family(Vito_Karleone,Jane_Karleone,Mustang);
        Man Dima = new Man("Dima","Karleonito",1981, (short) 60,new HashMap<String, String>(Arrays.stream(scheduleMichael).collect(Collectors.toMap(e -> e[0], e -> e[1]))),Jerry);
        Women Irina = new Women("Irina","Karleonito",1984, (short) 90,new HashMap<String, String>(Arrays.stream(scheduleMichael).collect(Collectors.toMap(e -> e[0], e -> e[1]))),Gold);
        Dima.setPet(Gold);
        Dima.greetPet(Dima.getPet());
        Irina.setPet(Jerry);
        Irina.greetPet(Irina.getPet());
        System.out.println(Dima.getSchedule());

        System.out.println("=============================2");
        Mafia.addChild(Iren);
        Mafia.addChild(Vadim);
        Mafia.addChild(Petya);
        Mafia.addChild(Michael);
        Mafia.deleteChild(1);
        Mafia.getChildren().get(0).displayInfo();

        System.out.println("=============================3");
        Mafia.addPet(Jerry);
        Mafia.deletePet(Jerry);
        System.out.println(Mafia);

        System.out.println("=============================4");
        show(Vito_Karleone, Jane_Karleone, Mustang);
    }

    public static void show(Human human1,Human human3,Pet pet1) {
        for (int i = 0; i < 10000; i++) {
            Family Mafia=new Family(human1,human3,pet1);
        }
    };
}





