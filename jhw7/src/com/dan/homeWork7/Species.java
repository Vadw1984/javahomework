package dan.homeWork7;

public enum Species {
    DOG,
    ROBOCAT,
    DOMESTICCAT,
    BIRD,
    FISH,
    UNKNOWN
}

