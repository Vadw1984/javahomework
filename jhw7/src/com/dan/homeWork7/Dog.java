package dan.homeWork7;

import java.util.Set;

class Dog extends Pet implements Foul {
    private Species species = Species.DOG;     // вид животного

    Dog(String nickname, int age, short trickLevel, Set<String> habit){
        super(nickname,age,trickLevel,habit);
//        this.nickname=nickname;
    }

    void respond(String nickname){

        System.out.printf("Привет, хозяин. Я %s. Меня зовут %s, Я машу хвостом!\n",species.name(),nickname);
    }
    @Override
    public void doFoul(){
        System.out.println("Нужно хорошо замести следы...\n");
    }

}
