package dan.homeWork7;

import java.util.HashSet;
import java.util.Set;

public abstract class Pet{
    private int age;            // возраст
    private String nickname;    // кличка
    private short trickLevel;   // уровень хитрости
    private String isTick;
    private Set<String> habits = new HashSet<String>();

    Pet(){

    }

    Pet(String nickname, int age, short trickLevel, Set<String> habits){
        this.nickname = nickname;
        this.age=age;
        this.trickLevel=trickLevel;
        this.habits = habits;
    }

    public void setTrickLevel(short trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String isTrick(){
        if(trickLevel>50){
            return isTick="очень хитрый";
        } else return isTick="почти не хитрый";
    }

    public String getNickname() {
        return nickname;
    }

    public Set<String> getHabits() {
        return habits;
    }

    public int getAge() {
        return age;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    public String getIsTick() {
        isTrick();
        return isTick;
    }

    void eat(){
        System.out.printf("Я кушаю!\n");
    }

    abstract void respond(String nickname);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pet)) return false;

        Pet pet = (Pet) o;

        if (age != pet.age) return false;
        return nickname.equals(pet.nickname);
    }

    @Override
    public int hashCode() {
        int result = age;
        result = 31 * result + nickname.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Pet {" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
//                ", species='" + species + '\'' +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits +
                '}';
    }
    @Override
    protected void finalize ( ) {
        System.out.println(this);
    }
}
