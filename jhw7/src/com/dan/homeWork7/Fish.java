package dan.homeWork7;

import java.util.Set;

public class Fish extends Pet {
    private Species species = Species.FISH;     // вид животного

    Fish(String nickname, int age, short trickLevel, Set<String> habit){
        super(nickname,age,trickLevel,habit);
    }

    void respond(String nickname){
        System.out.printf("Привет, хозяин. Я %s. Меня зовут %s, Я машу хвостом!\n",species.name(),nickname);
    }

}
