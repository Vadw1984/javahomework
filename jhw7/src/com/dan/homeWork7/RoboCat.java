package dan.homeWork7;

import java.util.Set;

public class RoboCat extends Pet implements Foul {
    private Species species = Species.ROBOCAT;     // вид животного

    RoboCat(String nickname, int age, short trickLevel, Set<String> habit){
        super(nickname,age,trickLevel,habit);
    }

    void respond(String nickname){
        System.out.printf("Привет, хозяин. Я %s. Меня зовут %s, Я машу хвостом!\n",species.name(),nickname);
    }

    @Override
    public void doFoul(){
        System.out.println("Нужно хорошо замести следы...\n");
    }
}
