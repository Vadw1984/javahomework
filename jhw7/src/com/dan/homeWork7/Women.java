package dan.homeWork7;

import java.util.Map;

final class Women extends Human {

    Women(String name, String surname, int year) {
        super(name, surname, year);
    }

    Women(String name, String surname, int year, short iq, Map<String,String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    Women(String name, String surname, int year, short iq, Map<String,String> schedule, Pet pet) {
        super(name, surname, year, iq, schedule);
    }
    public void makeup(){
        System.out.println("makeup");
    }

    @Override
    public void greetPet() {
        System.out.println("Привет ");
    }
}
