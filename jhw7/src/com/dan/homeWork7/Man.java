package dan.homeWork7;

import java.util.Map;

final class Man extends Human {

    Man(String name, String surname, int year){
        super(name,surname,year);
    }

    Man(String name, String surname,int year,short iq,Map<String,String> schedule){
        super(name,surname,year,iq,schedule);
    }

    Man(String name, String surname, int year, short iq, Map<String,String> schedule, Pet pet){
        super(name,surname,year,iq,schedule);
    }

    public void repairCar(){
        System.out.println("repairCar ");
    }

    @Override
    public void greetPet(Pet pet){
        System.out.printf("Привет, %s \n",pet.getNickname());
    }

}
