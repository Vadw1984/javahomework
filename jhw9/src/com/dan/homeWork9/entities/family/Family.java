package com.dan.homeWork9.entities.family;

import com.dan.homeWork9.entities.human.Human;
import com.dan.homeWork9.entities.pet.Pet;

import java.util.ArrayList;
import java.util.LinkedHashSet;

public class Family extends Human {

    final Human mother;                                               // mother
    final Human father;                                               // father
    private LinkedHashSet<Pet> pets = new LinkedHashSet<Pet>();                                                  // pet
    private ArrayList<Human> children = new ArrayList<Human>();

    public static int counterFamily = 2;

    public Family(Human father, Human mother) {
        this.mother = mother;
        this.father = father;
    }

    ;

    public ArrayList<Human> getChildren() {
        return children;
    }

    public Family(Human father, Human mother, Pet pet) {

        this.mother = mother;
        this.father = father;
        this.pets.add(pet);
    }

    ;

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public LinkedHashSet<Pet> getPets() {
        return pets;
    }

    public void addPet(Pet newPet) {
        pets.add(newPet);
    }

    public void deletePet(Pet newPet) {
        if (pets.contains(newPet)) {
            pets.remove(newPet);
        } else {
            System.out.println("there is no this Pet");
        }
    }

    public void addChild(Human newChild) {
        children.add(newChild);
    }

    public boolean deleteChild(int index) {
        if (children.size() <= index) {
            System.out.println("there is no this index");
            return false;
        } else {
            children.remove(index);
            return true;
        }
    }


    public void deleteChild(Human human) {
        if (children.contains(human)) {
            children.remove(human);
        } else {
            System.out.println("there is no this object");
        }
    }

    public int countFamily() {
        return counterFamily += children.size();
    };

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", pet=" + pets +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;
        if (!super.equals(o)) return false;

        Family family = (Family) o;

        if (mother != null ? !mother.equals(family.mother) : family.mother != null) return false;
        return father != null ? father.equals(family.father) : family.father == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (mother != null ? mother.hashCode() : 0);
        result = 31 * result + (father != null ? father.hashCode() : 0);
        return result;
    }

    @Override
    protected void finalize() {
        System.out.println(this);
    }
}