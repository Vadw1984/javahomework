package com.dan.homeWork9.entities.human;

import com.dan.homeWork9.entities.family.Family;
import com.dan.homeWork9.entities.pet.Pet;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;

public abstract class Human{
    private String name;                                                // имя
    private String surname;                                             // фамилия
    private int year;                                                   // возраст
    private short iq;                                                   // iq
    private Map<String,String> schedule = new HashMap<>();              // schedule
    private Family family;
    private Pet pet;
    private LinkedHashSet<Pet> pets = new LinkedHashSet<Pet>();



    public Human(){

    }

    public Human(String name, String surname,int year){
        this.name = name;
        this.surname = surname;
        this.year=year;
        this.iq = 130;

    }

    public Human(String name, String surname,int year,short iq,Map<String,String> schedule){
        this.name = name;
        this.surname = surname;
        this.year=year;
        this.iq=iq;
        this.schedule=schedule;
    }

    public Human(String name, String surname, int year, short iq, Map<String,String> schedule, Family family, Pet pet){
        this.name = name;
        this.surname = surname;
        this.year=year;
        this.iq=iq;
        this.schedule=schedule;
        this.family=family;
        this.pets.add(pet);
    }

    public Family getFamily() {return family;}

    public Pet getPet() {return pet;}

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    public short getIq() {
        return iq;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
        pets.add(pet);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setIq(short iq) {
        this.iq = iq;
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    public void  displayInfo(){
        System.out.printf("Name: %s \tAge: %d\n", this.name, this.year);
    }

    public int  displayFamilyCount(){
       return Family.counterFamily;
    }

    public void greetPet(Pet pet) {
        Iterator<Pet> iter = pets.iterator();
        iter.forEachRemaining(e->System.out.println(e));
        System.out.printf("Привет, %s \n",pet.getNickname());
    }

    public void greetPet(){};

    @Override
    public String toString() {

        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + schedule+
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;

        Human human = (Human) o;

        if (year != human.year) return false;
        if (iq != human.iq) return false;
        if (name != null ? !name.equals(human.name) : human.name != null) return false;
        if (surname != null ? !surname.equals(human.surname) : human.surname != null) return false;
        if (schedule != null ? !schedule.equals(human.schedule) : human.schedule != null) return false;
        if (family != null ? !family.equals(human.family) : human.family != null) return false;
        return pet != null ? pet.equals(human.pet) : human.pet == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + year;
        result = 31 * result + (int) iq;
        result = 31 * result + (schedule != null ? schedule.hashCode() : 0);
        result = 31 * result + (family != null ? family.hashCode() : 0);
        result = 31 * result + (pet != null ? pet.hashCode() : 0);
        return result;
    }

    @Override
    protected void finalize ( ) {
        System.out.println(this.toString());
    }

}