package com.dan.homeWork9.entities.pet;

public enum Species {
    DOG,
    ROBOCAT,
    DOMESTICCAT,
    BIRD,
    FISH,
    UNKNOWN
}

