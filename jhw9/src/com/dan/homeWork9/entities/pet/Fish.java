package com.dan.homeWork9.entities.pet;

import java.util.Set;

public class Fish extends Pet {
    private Species species = Species.FISH;     // вид животного

    public Fish(String nickname, int age, short trickLevel, Set<String> habit){
        super(nickname,age,trickLevel,habit);
    }

    public void respond(String nickname){
        System.out.printf("Привет, хозяин. Я %s. Меня зовут %s, Я машу хвостом!\n",species.name(),nickname);
    }

}
