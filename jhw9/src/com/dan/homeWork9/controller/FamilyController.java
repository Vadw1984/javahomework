package com.dan.homeWork9.controller;

import com.dan.homeWork9.entities.family.Family;
import com.dan.homeWork9.entities.human.Human;
import com.dan.homeWork9.entities.pet.Pet;
import com.dan.homeWork9.service.FamilyService;

import java.util.LinkedHashSet;
import java.util.List;

public class FamilyController {
    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void saveFamily(Family family) {
        familyService.saveFamily(family);
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public void getFamiliesBiggerThan(int num) {
        familyService.getFamiliesBiggerThan(num);
    }

    public void getFamiliesLessThan(int num) {
        familyService.getFamiliesLessThan(num);
    }

    public void countFamiliesWithMemberNumber(int num) {
        familyService.countFamiliesWithMemberNumber(num);
    }

    public void createNewFamily(Human human1, Human human2) {
        familyService.createNewFamily(human1, human2);
    }

    public void deleteFamilyByIndex(int index) {
        familyService.deleteFamilyByIndex(index);
    }
    public void deleteFamilyByObject(Family family) {
        familyService.deleteFamilyByObject(family);
    }

    public Family bornChild(Family family, String boyName, String girlName) {
        return familyService.bornChild(family, boyName, girlName);
    }

    public Family adoptChild(Family family, Human human) {
        return familyService.adoptChild(family, human);
    }

    public void deleteAllChildrenOlderThen(List<Family> familyList, int age) {
        familyService.deleteAllChildrenOlderThen(familyList, age);
    }

    public int count(List<Family> familyList) {
        return familyService.count(familyList);
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    public LinkedHashSet<Pet> getPets(List<Family> familyList, int index) {
        return familyService.getPets(familyList, index);
    }

    public void addPet(List<Family> familyList, int index, Pet pet) {
        familyService.addPet(familyList, index, pet);
    }

}
