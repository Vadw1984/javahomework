package com.dan.homeWork9.dao;

import com.dan.homeWork9.entities.family.Family;

import java.util.ArrayList;
import java.util.List;


public class CollectionFamilyDao implements FamilyDao {
    private List<Family> FamilyList = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
//        return Collections.unmodifiableList(FamilyList);
        return FamilyList;
    }

    @Override
    public Family getFamilyById(int index) {
        if(index>=FamilyList.size()){
            return null;
        }else
        return FamilyList.get(index);
    }

    @Override
    public void saveFamily(Family family) {
        FamilyList.add(family);
    }

    @Override
    public void deleteFamilyByIndex(int index) {
        FamilyList.remove(index);
    }

    @Override
    public boolean deleteFamily(Family family) {
        return FamilyList.remove(family);
    }
}
