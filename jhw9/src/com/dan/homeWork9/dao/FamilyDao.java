package com.dan.homeWork9.dao;

import com.dan.homeWork9.entities.family.Family;

import java.util.List;

public interface FamilyDao {
    List<Family> getAllFamilies();

    Family getFamilyById(int index);

    void saveFamily(Family family);

    void deleteFamilyByIndex(int index);

    boolean deleteFamily(Family family);
}
