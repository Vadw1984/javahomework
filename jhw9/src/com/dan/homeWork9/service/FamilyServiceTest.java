package com.dan.homeWork9.service;

import com.dan.homeWork9.dao.CollectionFamilyDao;
import com.dan.homeWork9.dao.FamilyDao;
import com.dan.homeWork9.entities.family.Family;
import com.dan.homeWork9.entities.human.Man;
import com.dan.homeWork9.entities.human.Women;
import com.dan.homeWork9.entities.pet.Dog;
import com.dan.homeWork9.entities.pet.DomesticCat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashSet;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FamilyServiceTest {

    Man Petya = new Man("Petya", "Zrorodny", 5);
    Women Jane_Karleone = new Women("Jane", "Karleone", 67);
    Man Vito_Karleone = new Man("Vito", "Karleone", 65);

    Dog Mustang = new Dog("Mustang ", 7, (short) 60, new HashSet<String>(Arrays.asList("eat", "sleep", "drink")));
    DomesticCat Tom = new DomesticCat("Tom", 3, (short) 40, new HashSet<String>(Arrays.asList("eat", "sleep", "drink")));

    Family Mafia = new Family(Vito_Karleone, Jane_Karleone, Mustang);

    FamilyDao familyDao = new CollectionFamilyDao();
    private FamilyService familyService = new FamilyService(familyDao);

    @BeforeEach
    void setUp() {
        familyService.saveFamily(Mafia);
    }

    @Test
    void getAllFamilies() {
        assertFalse(familyService.getAllFamilies().isEmpty());
    }

    @Test
    void saveFamily() {
        assertEquals(Mafia, familyService.getAllFamilies().get(0));
    }

    @Test
    void displayAllFamilies() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        familyService.displayAllFamilies();
        String expectedOutput = "" + Mafia + "";

        assertTrue(outContent.toString().contains(expectedOutput));
    }

    @Test
    void getFamiliesBiggerThan() {
        Mafia.addChild(Petya);
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        familyService.getFamiliesBiggerThan(2);

        String expectedOutput = "" + Mafia + "";

        assertTrue(outContent.toString().contains(expectedOutput));
    }

    @Test
    void getFamiliesLessThan() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        familyService.getFamiliesLessThan(3);

        String expectedOutput = "" + Mafia + "";

        assertTrue(outContent.toString().contains(expectedOutput));
    }

    @Test
    void countFamiliesWithMemberNumber() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        familyService.countFamiliesWithMemberNumber(2);

        String expectedOutput = "" + Mafia + "";

        assertTrue(outContent.toString().contains(expectedOutput));
    }

    @Test
    void createNewFamily() {
        familyService.createNewFamily(Vito_Karleone, Jane_Karleone);
        familyService.getAllFamilies().get(0).addPet(Mustang);
        assertEquals(Mafia, familyService.getAllFamilies().get(0));
    }

    @Test
    void deleteFamilyByIndex() {
        familyService.deleteFamilyByIndex(0);
        assertTrue(familyService.getAllFamilies().isEmpty());
    }

    @Test
    void deleteFamilyByObject() {
        familyService.deleteFamilyByObject(Mafia);
        assertTrue(familyService.getAllFamilies().isEmpty());
    }

    @Test
    void bornChild() {
        familyService.bornChild(Mafia,"Vlad","Kristina");
        assertTrue(familyService.getAllFamilies().get(0).getChildren().size()>0);
    }

    @Test
    void adoptChild() {
        familyService.adoptChild(Mafia,Petya);
        assertTrue(familyService.getAllFamilies().get(0).getChildren().size()>0);
    }

    @Test
    void deleteAllChildrenOlderThen() {
        familyService.adoptChild(Mafia,Petya);
        familyService.deleteAllChildrenOlderThen(familyService.getAllFamilies(),3);
        assertTrue(familyService.getAllFamilies().get(0).getChildren().size()==0);
    }

    @Test
    void count() {
        assertEquals(1,familyService.count(familyService.getAllFamilies()));
    }

    @Test
    void getFamilyById() {
        assertEquals(Mafia,familyService.getFamilyById(0));
    }

    @Test
    void getPets() {
        assertTrue(familyService.getFamilyById(0).getPets().contains(Mustang));
    }

    @Test
    void addPet() {
        Mafia.addPet(Tom);
        assertTrue(familyService.getFamilyById(0).getPets().contains(Tom));
    }
}