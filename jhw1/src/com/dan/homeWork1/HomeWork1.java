package com.dan.homeWork1;

import java.util.Random;
import java.util.Scanner;

public class HomeWork1 {

    public static void main(String[] args) {

        System.out.println( "Let the game begin!");
        String name=getName();
        startGame(name);

    }

    public static void startGame(String name) {
        int userNum= (int) getUserNumber(name);

          System.out.printf("Your number: %d \n",userNum );

        int randomNum = (int) getRandomNumber();
        System.out.printf("Random number: %d \n",randomNum );

        compareNumbers(userNum,randomNum,name);
    };
        public static String getName() {
        Scanner in = new Scanner(System.in);
        System.out.print("Input your name: ");
        String name = in.nextLine();
        return name;
    };

    public static Number getUserNumber(String name) {
        Scanner in = new Scanner(System.in);
        System.out.printf("\n What is your number %s? = ", name);
        int userNum = in.nextInt();
//        in.close();
        return userNum;
    };

    public static Number getRandomNumber() {
        Random random  = new Random();
        int randomNum = random.nextInt(100+1);
        return randomNum;
    }

    public static void compareNumbers(int userNum, int randomNum,String name) {
             if(userNum>randomNum){
                System.out.print("Your number is too big. Please, try again.");
                startGame(name);
            } else if(userNum<randomNum) {
                System.out.print("Your number is too small. Please, try again.");
                 startGame(name);
            }else System.out.printf("Congratulations, %s!",name);
       }

}


